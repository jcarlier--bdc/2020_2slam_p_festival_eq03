<?php
namespace modele\dao;

use modele\metier\Representation;
use modele\metier\Groupe;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;
use \PDOStatement;
use PDO;

class RepresentationDAO {

    // Méthode qui crée un objet métier à partir d'un enregistrement
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $idGroupe = $enreg['IDGROUPE'];
        $idLieu = $enreg['IDLIEU'];
        $dateRep = $enreg['DATEREPRES'];
        $heureDebut = $enreg['HEUREDEBUT'];
        $heureFin = $enreg['HEUREFIN'];
       
        $objetLieu = LieuDAO::getOneById($idLieu);
        $objetGroupe = GroupeDAO::getOneById($idGroupe);
  
        $uneRepresentation = new Representation($id,$dateRep, $objetLieu , $objetGroupe, $heureDebut, $heureFin);
        return $uneRepresentation;
    }
    
    // Méthode qui crée un enregistrement à partir d'un objet métier
    protected static function metierVersEnreg(Representation $uneRepresentation,  PDOStatement $stmt) {

        $stmt->bindValue(':id', $uneRepresentation->getId());
        $stmt->bindValue(':idLieu', $uneRepresentation->getLeLieu()->getId());
        $stmt->bindValue(':idGroupe', $uneRepresentation->getLeGroupe()->getId());
        $stmt->bindValue(':dateRepres', $uneRepresentation->getDateRep());
        $stmt->bindValue(':heureDebut', $uneRepresentation->getHeureDebut());
        $stmt->bindValue(':heureFin', $uneRepresentation->getHeureFin());
    }
    
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY idLieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // pour chaque enregistrement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    // recherche d'une représentation selon la valeur de son identifiant ; retourne la représentation trouvée
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();

        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    // On récupère toutes les dates de représentations triées par ordre croissant
    public static function getAllDates() {
        $lesDates = array();
        $requete = "SELECT DISTINCT dateRepres FROM Representation order by dateRepres";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {

            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
  
                $lesDates[] = self::enregVersDate($enreg);
            }
        }
        return $lesDates;
    }
    
    protected static function enregVersDate(array $enreg) {
        $dateRep[] = $enreg['DATEREPRES'];
  
        return $dateRep;
    }
    
    /**
     * Détruire un enregistrement de la table REPRESENTATION d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
    
    
    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation VALUES (:id, :idGroupe, :idLieu, :dateRepres, :heureDebut, :heureFin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        // on retourne l'objet
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour un enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Representation $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE Representation SET idGroupe=:idGroupe, idLieu=:idLieu , dateRepres=:dateRepres , heureDebut=:heureDebut, heureFin=:heureFin
           WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    
    /**
     * Permet de vérifier s'il existe ou non une representation ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de la representation à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
    
    
}