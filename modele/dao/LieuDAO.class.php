<?php

namespace modele\dao;

use modele\metier\Lieu;
use PDO;

/**
 * Description of LieuDAO
 * Classe métier :  Lieu
 * @author eleve
 * @version 2020
 */
class LieuDAO {
    
    /**
     * crée un objet prestation à partir d'un enregistrement
     * @param array $enreg
     * @return Lieu objet prestation obtenu
     */
     protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $nom = $enreg['NOM'];
        $adr = $enreg['ADRESSE'];
        $CapAcceuil = $enreg['CAPACITE'];
        $objetMetier = new Lieu($id, $nom, $adr, $CapAcceuil);
        return $objetMetier;
    }
    
    /**
     * Retourne la liste de tous les lieux 
     * @return array tableau d'objets de lieu
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    /**
     * Recherche un lieu selon la valeur de son identifiant
     * @param string $id
     * @return Lieu le lieu trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE ID = :ID";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':ID', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    public static function getOneByNom($nom) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE NOM = :nom";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':nom', $nom);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
}
