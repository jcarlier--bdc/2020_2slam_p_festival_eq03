<?php
namespace modele\metier;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Lieu{
    
    /**
     * id du lieu
     * @var string
     */
    private $id;
    
    /**
     * nom du lieu
     * @var string
     */
    private $nom;
    
    /**
     * adresse du lieu
     * @var string
     */
    private $adresse;
    
    /**
     * capacité d'accueil du lieu
     * @var integer
     */
    private $capacite;
    
    // constructeur
    function __construct(string $id, string $nom, string $adresse, int $capacite) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->capacite = $capacite;
    }

    
    // ACCESSEURS ET MUTATEURS 
    
    function getId(): string {
        return $this->id;
    }

    function setId(string $id) {
        $this->id = $id;
    }

    function getNom(): string {
        return $this->nom;
    }

    function getAdresse(): string {
        return $this->adresse;
    }

    function getCapacite(): string {
        return $this->capacite;
    }

    function setNom(string $nom) {
        $this->nom = $nom;
    }

    function setAdresse(string $adresse) {
        $this->adresse = $adresse;
    }

    function setCapacite(string $capacite) {
        $this->capacite = $capacite;
    }


  
}

