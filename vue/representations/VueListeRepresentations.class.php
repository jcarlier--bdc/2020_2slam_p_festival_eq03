<?php
namespace vue\representations;

use vue\VueGenerique;
use modele\dao\RepresentationDAO;
use modele\dao\Bdd;
use controleur\CtrlRepresentations;

/**
 * Description Page d'affichage des listes des representations
 * @author Alexia
 * @version 2020
 */
class VueListeRepresentations extends VueGenerique{


    /** @var array liste des Representation */
    private $lesRepresentations;

    /** @var array liste des différent Lieux */
    private $lesLieux;
    
    /** @var array liste des groupes */
    private $lesGroupes;
    
    /** @var array liste des dates de représentations */
    private $lesDates;
    
    
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();

        
        // IL FAUT QU'IL Y AIT AU MOINS UNE REPRESENTATION POUR QUE L'AFFICHAGE SOIT EFFECTUÉ 
        if (count($this->lesRepresentations) != 0) {
            // Affichage des représentations, par date et par lieux

            $lesDates = RepresentationDAO::getAllDates();

            if (is_array($lesDates)) {

                foreach ($lesDates as $uneDate) {
?>
                                    
                                    <strong><?= implode($uneDate) ?></strong><br>
                                    
                                   
                                    <table width="45%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                                        <!--AFFICHAGE DE LA LIGNE D'EN-TÊTE-->
                                        <tr class="enTeteTabQuad">
                                            <td width="25%">Lieu</td>
                                            <td width="25%">Groupe</td>
                                            <td width="25%">Heure Debut</td>
                                            <td width="25%">Heure Fin</td>
                                            <td width="25%"></td>
                                            <td width="25%"></td>
                                        </tr>
                    <?php foreach ($this->lesRepresentations as $uneRepresentation) { ?>
                                                <tr class="ligneTabQuad">
                                                    
                        <?php if (($uneRepresentation->getDateRep()) == (implode($uneDate))) { ?>

                            <td><?= $uneRepresentation->getLeLieu()->getNom() ?></td>
                            <td><?= $uneRepresentation->getLeGroupe()->getNom() ?></td>
                            <td><?= $uneRepresentation->getHeureDebut() ?></td>
                            <td><?= $uneRepresentation->getHeureFin() ?></td>
                            <td width="16%" align="center" > 
                            <a href="index.php?controleur=representations&action=modifier&id=<?= $uneRepresentation->getId() ?>" >
                            Modifier
                            </a>
                            </td>
                            <td width="16%" align="center" > 
                            <a href="index.php?controleur=representations&action=supprimer&id=<?= $uneRepresentation->getId() ?>" >
                            Supprimer
                            </a>
                            </td>
                        </tr>
                            <?php
                        }
                    }
                            ?></table><br><?php
                }
            }
        }?>
         <!-- liens de redirection vers la création de representation -->
                    <a href='index.php?controleur=representations&action=creer'>
                        Création d'une Representation
                    </a>
         <?php
        include $this->getPied();
    }

    public function setLesRepresentations(array $lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }   
}
