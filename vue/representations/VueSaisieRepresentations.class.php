<?php

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\dao\RepresentationDAO;
use controleur\CtrlRepresentations;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;


class VueSaisieRepresentations extends VueGenerique {

    /** @var Representation representation à modifier */
    private $uneRepresentation;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;

    /** @var string à afficher en tête du tableau */
    private $message;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete(); ?>
        <!-- Formulaire de création/ modification du groupe : -->
        <form method="POST" action="index.php?controleur=representations&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="40%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>
                <?php
                // En cas de création, l'id est accessible sinon l'id est dans un champs caché
                
                if ($this->actionRecue == "creer") {
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->getUneRepresentation()->getId() ?>" name="id" size ="2"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr class="autreLigne">
                        <td><input type="hidden" value="<?= $this->getUneRepresentation()->getId() ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>

                <tr class="ligneTabNonQuad">
                    <td> Date*: </td>
                    <td><input type="date" value="<?= $this->getUneRepresentation()->getDateRep() ?>" name="date" size="30" maxlength="25"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Lieu*: </td>
                    <!-- Liste déroulante -->
                    <td><SELECT name="lieu" size="1">

                            <?php
                            //recuperation de tous les lieux de la base
                            $lesLieux = LieuDAO::getAll();
                            //pour chaque lieu de la base:
                            foreach ($lesLieux as $unLieu) {
                                ?>
                            <!-- si le lieu correspont au groupe de la représentation , le pré-séléctionner sinon ajouter le résultat dans la liste -->
                                <OPTION <?php if($unLieu->getId() == $this->getUneRepresentation()->getLeLieu()->getId()){
                                    // pré-sélection du lieu :
                                    echo 'selected="selected"';
                                }?>>
                                <!-- Affichage du nom du Lieu -->
                                <?php echo $unLieu->getNom() ?>
                                </OPTION>;

                            <?php } ?>

                        </SELECT>
                    </td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Groupe*: </td>
                    <!-- Liste déroulante -->
                    <td><SELECT name="groupe" size="1">

                            <?php
                            //récuperation de tous les groupes de la base:
                            $lesGroupes = GroupeDAO::getAll();
                            //pour chaque groupe dans la base:
                            foreach ($lesGroupes as $unGroupe) {
                                
                               ?>
                            <!-- si le groupe correspont au groupe de la representation , le pré-séléctionner -->
                                <OPTION <?php if($unGroupe->getId() == $this->getUneRepresentation()->getLeGroupe()->getId()){
                                    // pré-sélection du Groupe
                                    echo 'selected="selected"';
                                    
                                }?>>
                                    <!-- Affichage du nom du Groupe -->
                                <?php echo $unGroupe->getNom() ?>
                                </OPTION>;

                            <?php } ?>

                        </SELECT>
                    </td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de début*: </td>
                    <td><input type="time" value="<?= $this->getUneRepresentation()->getHeuredebut() ?>" name="heuredebut" size="30" maxlength="25" placeholder="HH:MM:SS"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure de Fin*: </td>
                    <td><input type="time" value="<?= $this->getUneRepresentation()->getHeurefin() ?>" name="heurefin" size="30" maxlength="25" placeholder="HH:MM:SS"></td>
                </tr>
                <tr class="ligneTabNonQuad">

                </tr>
            </table>
            <!-- Autre tableau contenant les boutons: -->
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <!-- validation du formulaire -->
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <!-- annulation du formulaire -->
                    <td align="left"><input type="reset" value="Annuler" name="annuler"> </td>
                </tr>
            </table>
            <!-- bouton retour -->
            <a href="index.php?controleur=representations&action=consulter">Retour</a>
        </form>       
        <?php include $this->getPied();
    }

    function getUneRepresentation(): Representation {
        return $this->uneRepresentation;
    }

    function getActionRecue(): string {
        return $this->actionRecue;
    }

    function getActionAEnvoyer(): string {
        return $this->actionAEnvoyer;
    }

    function getMessage(): string {
        return $this->message;
    }

    function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }

    function setActionRecue(string $actionRecue) {
        $this->actionRecue = $actionRecue;
    }

    function setActionAEnvoyer(string $actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }

    function setMessage(string $message) {
        $this->message = $message;
    }


}