<?php

namespace vue\groupe;

use vue\VueGenerique;
use modele\metier\Groupe;

/**
 * Description Page de saisie/modification d'un groupe donné
 * @author Alexia
 * @version 2020
 */
class VueSaisieGroupe extends VueGenerique {
    /** @var Groupe groupe à modifier */
    private $unGroupe;
    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;
    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;
    /** @var string à afficher en tête du tableau */
    private $message;
    // constructeur
    public function __construct() {
        parent::__construct();
    }
    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=groupes&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="40%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>
                <?php
                // En cas de création, l'id est accessible sinon l'id est dans un champ
                // caché
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->getUnGroupe()->getId() 
                            ?>" name="id" size ="2"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché 
                    ?>
                    <tr class="autreLigne">
                        <td><input type="hidden" value="<?= $this->getUnGroupe()->getId() 
                            ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?>

                <tr class="ligneTabNonQuad">
                    <td> Nom*: </td>
                    <td><input type="text" value="<?= $this->getUnGroupe()->getNom() ?>" name="nom" 
                               size="30" maxlength="40"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Identité Responsable*: </td>
                    <td><input type="text" value="<?= $this->getUnGroupe()->getIdentite() ?>" name="identite" 
                               size="30" maxlength="25"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Adresse postale*: </td>
                    <td><input type="text" value="<?= $this->getUnGroupe()->getAdresse() ?>" name="adresse" 
                               size="30" maxlength="25"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Nombre de Personne*: </td>
                    <td><input type="number" value="<?= $this->getUnGroupe()->getNbPers() ?>" name="nombre" 
                               size="30" maxlength="25"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Nom du pays*: </td>
                    <td><input type="text" value="<?= $this->getUnGroupe()->getNomPays() ?>" name="nomPays" 
                               size="30" maxlength="25"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Hebergement*: </td>
                    <td><input type="text" value="<?= $this->getUnGroupe()->getHebergement() ?>" name="hebergement" 
                               size="1" maxlength="1"></td>
                </tr>
            </table>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler"> </td>
                </tr>
            </table>
            <a href="index.php?controleur=groupes&action=liste">Retour</a>
        </form>        
        <?php
        include $this->getPied();
    }

    public function getUnGroupe(): Groupe {
        return $this->unGroupe;
    }

    public function getActionRecue() {
        return $this->actionRecue;
    }

    public function getActionAEnvoyer() {
        return $this->actionAEnvoyer;
    }

    public function getMessage() {
        return $this->message;
    }

    public function setUnGroupe(Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }

    public function setActionRecue($actionRecue) {
        $this->actionRecue = $actionRecue;
    }

    public function setActionAEnvoyer($actionAEnvoyer) {
        $this->actionAEnvoyer = $actionAEnvoyer;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

}