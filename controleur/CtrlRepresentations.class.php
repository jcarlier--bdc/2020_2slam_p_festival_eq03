<?php
namespace controleur;

use modele\dao\RepresentationDAO;
use modele\dao\LieuDAO;
use modele\dao\GroupeDAO;
use modele\metier\Representation;
use modele\dao\Bdd;
use vue\representations\VueListeRepresentations;
use vue\representations\VueSaisieRepresentations;
use vue\representations\VueSupprimerRepresentations;


class CtrlRepresentations extends ControleurGenerique {

    /** controleur= representation & action= defaut
     * par défaut, afficher la liste des representations      */
    public function defaut() {
        $this->consulter();
    }
    
    /** controleur= representations & action= consulter
     * Afficher la liste des representations       */
    function consulter() {
        $laVue = new VueListeRepresentations();
        $this->vue = $laVue;
        // La vue a besoin de la liste des representationss
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representations");
        $this->vue->afficher();
    }

    /** controleur= representations & action= modifier & id = identifiant de la representation visée
     * Modifier les representations  */
    function modifier() {
        // Lecture de l'id de la representation 
        $idRepres = $_GET['id'];
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Connexion à la bdd   
        Bdd::connecter();
       $laVue->setUneRepresentation(RepresentationDAO::getOneById($idRepres));
       $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier la representation : " . $laVue->getUneRepresentation()->getId());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representations ");
       $this->vue->afficher();
    }
    
  
    
    /** controleur= representations & action=supprimer & id=identifiant_representation
     * Supprimer une representation d'après son identifiant     */
    public function supprimer() {
        $idRepres = $_GET["id"];
        $laVue = new VueSupprimerRepresentations();
        $this->vue = $laVue;
        // Lire dans la BDD la representation à supprimer
        Bdd::connecter();
        $laVue->setUneRepresentation(RepresentationDAO::getOneById($idRepres));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representations");
        $this->vue->afficher();
    }
    
    /** controleur= representations & action= validerSupprimer & id = n° representation
     * supprimer une representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de la representation à supprimer");
        } else {
            // suppression de la representation d'après son identifiant
            RepresentationDAO::delete($_GET["id"]);
        }
        // retour à la liste des representation
        header("Location: index.php?controleur=representations&action=consulter");
    }
    


    /** controleur= representation & action=creer
     * Afficher le formulaire de création d'une representation     */
    public function creer() {
        $laVue = new VueSaisieRepresentations();
        $this->vue = $laVue;
        // Lire dans la BDD les données de la representation à modifier
        Bdd::connecter();
        // Récupération du 1er groupe et du 1er lieux, pour que ce ne soit pas vide
        $groupe = GroupeDAO::getOneById('g001');
        $lieu = LieuDAO::getOneById('001');
        $laVue->setUneRepresentation(new Representation("","" ,$lieu, $groupe, "", ""));
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Créer une nouvelle Representation");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }
    
    /**
     * Vérification de la saisie des données du formulaire
     * @param Representation $uneRepresentation
     * @param bool $creation
     */
    private function verifierDonneesRepresentation(Representation $uneRepresentation, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if ($creation && $uneRepresentation->getId() == "" || $uneRepresentation->getLeGroupe()->getId() == ""|| $uneRepresentation->getLeLieu()->getId() == ""||
                $uneRepresentation->getDateRep() == ""|| $uneRepresentation->getHeureDebut() == ""|| $uneRepresentation->getHeureFin() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $uneRepresentation->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($uneRepresentation->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (RepresentationDAO::isAnExistingId($uneRepresentation->getId())) {
                    GestionErreurs::ajouter("La Representation " . $uneRepresentation->getId() . " existe déjà");
                }
            }
        }
        
    }
    
        /** controleur= representation & action=validerModifier
     * modifier une representation la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        // Récupération des données 
        $nomLieu = $_REQUEST['lieu'];
        $nomLieu = LieuDAO::getOneByNom($nomLieu);
        $nomGroupe = GroupeDAO::getOneByNom($_REQUEST['groupe']);
        
        /* @var Representation $uneRepresentation  : récupération du contenu du formulaire et instanciation d'une Representation */
        $uneRepresentation = new Representation($_REQUEST['id'], $_REQUEST['date'], $nomLieu, $nomGroupe,$_REQUEST['heuredebut'], $_REQUEST['heurefin']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesRepresentation($uneRepresentation, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications
            RepresentationDAO::update($uneRepresentation->getId(), $uneRepresentation);
            // revenir à la liste des Representation
            header("Location: index.php?controleur=representations&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRepresentation);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier la Representation : " . $laVue->getUneRepresentation()->getId(). 
                    " (" . $laVue->getUneRepresentation()->getId() . ")");
            $this->vue->setTitre("Festival - Representation");
            parent::controlerVueAutorisee();
            $this->vue->afficher();
        }
    }
    
    /** controleur= representation & action=validerCreer
     * création d'une representation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Representation $uneRepresentation  : récupération du contenu du formulaire et instanciation d'une Representation */
        $uneRepresentation = new Representation($_REQUEST['id'], $_REQUEST['date'], LieuDAO::getOneByNom($_REQUEST['lieu']), 
                GroupeDAO::getOneByNom($_REQUEST['groupe']), $_REQUEST['heuredebut'], $_REQUEST['heurefin']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesRepresentation($uneRepresentation, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications
            RepresentationDAO::insert($uneRepresentation);
            // revenir à la liste des Representation
            header("Location: index.php?controleur=representations&action=consulter");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieRepresentations();
            $this->vue = $laVue;
            $laVue->setUneRepresentation($uneRepresentation);
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Créer une nouvelle Representation");
            $this->vue->setTitre("Festival - Representation");
            parent::controlerVueAutorisee();
            $this->vue->afficher();
        }
    }
    
}
