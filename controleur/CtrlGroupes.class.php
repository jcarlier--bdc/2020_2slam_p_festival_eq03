<?php
namespace controleur;

use modele\dao\GroupeDAO;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\groupe\VueListeGroupe;
use vue\groupe\VueSaisieGroupe;
use vue\groupe\VueSupprimerGroupe;
use vue\groupe\VueDetailGroupe;


class CtrlGroupes extends ControleurGenerique {

    /** controleur= groupe & action= defaut
     * par défaut, afficher la liste des groupes      */
    public function defaut() {
        $this->liste();
    }

    /** controleur= groupe & action= liste
     * Afficher la liste des groupe de chambres       */
    function liste() {
        $laVue = new VueListeGroupe();
        $this->vue = $laVue;
        // On récupère un tableau composé d'objets de type Groupe lus dans la BDD
        Bdd::connecter();
        $laVue->setGroupe(GroupeDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }

/** controleur= groupe & action=detail & id=identifiant_groupe
     * Afficher un groupe d'après son identifiant     */
    public function detail() {
        $idGroupe = $_GET["id"];
        $this->vue = new VueDetailGroupe();
        // Lire dans la BDD les données du Groupe à afficher
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupe));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupe");
        $this->vue->afficher();
    }

    /** controleur= groupe & action=modifier & id = n° groupe
     * Afficher le formulaire de modification d'un groupe     */
    public function modifier() {
        $idGroupe = $_GET["id"];
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        // Lire dans la BDD les données du groupe à modifier
        Bdd::connecter();
        $laVue->setUnGroupe(GroupeDAO::getOneById($idGroupe));
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le groupe : " . $laVue->getUnGroupe()->getId());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }

    /** controleur= groupe & action=creer
     * Afficher le formulaire de création d'un groupe     */
    public function creer() {
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        // Lire dans la BDD les données du groupe à modifier
        Bdd::connecter();
        $laVue->setUnGroupe(new Groupe("", "","" ,"", "", "", ""));
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Créer un nouveau Groupe");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }

    
    /** controleur= groupe & action=supprimer & id=identifiant_groupe
     * Supprimer un groupe d'après son identifiant     */
    public function supprimer() {
        $idGr = $_GET["id"];
        $laVue = new VueSupprimerGroupe();
        $this->vue = $laVue;
        // Lire dans la BDD le groupe à supprimer
        Bdd::connecter();
        $laVue->setUnGroupe(GroupeDAO::getOneById($idGr));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }

    
        /** controleur= groupe & action=validerModifier
     * modifier un groupe dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un Groupe */
        $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identite'], $_REQUEST['adresse'], 
                $_REQUEST['nombre'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesGroupe($unGroupe, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications
            GroupeDAO::update($unGroupe->getId(), $unGroupe);
            // revenir à la liste des Groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGroupe);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier le Groupe : " . $laVue->getUnGroupe()->getNom()
                    . " (" . $laVue->getUnGroupe()->getNom() . ")");
            $this->vue->setTitre("Festival - Groupes");
            parent::controlerVueAutorisee();
            $this->vue->afficher();
        }
    }
    
    /** controleur= groupe & action=validerCreer
     * création d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Groupe $unGroupe  : récupération du contenu du formulaire et instanciation d'un groupe */
       $unGroupe = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identite'], 
               $_REQUEST['adresse'], $_REQUEST['nombre'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesGroupe($unGroupe, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les informations d'un groupe
            GroupeDAO::insert($unGroupe);
            // revenir à la liste des Groupes
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de saisie
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGroupe);
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Créer un nouveau Groupe");
            $this->vue->setTitre("Festival - Groupes");
            parent::controlerVueAutorisee();
            $this->vue->afficher();
        }
    }

    /** controleur= groupes & action= validerSupprimer & id = n° groupe
     * supprimer un groupee dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du groupe à supprimer");
        } else {
            // suppression du groupe d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des Groupe
        header("Location: index.php?controleur=groupes&action=liste");
    }

 /**
     * Vérification de la saisie des données du formulaire
     * @param Groupe $unGroupe
     * @param bool $creation
     */
    private function verifierDonneesGroupe(Groupe $unGroupe, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if ($creation && $unGroupe->getId() == "" || $unGroupe->getNom() == ""|| $unGroupe->getIdentite() == ""|| 
                $unGroupe->getAdresse() == ""|| $unGroupe->getNbPers() == ""|| $unGroupe->getNomPays() == ""|| 
                $unGroupe->getHebergement() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unGroupe->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($unGroupe->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (GroupeDAO::isAnExistingId($unGroupe->getId())) {
                    GestionErreurs::ajouter("Le Groupe " . $unGroupe->getId() . " existe déjà");
                }
            }
        }
        if ($unGroupe->getNom() != "" && GroupeDAO::isAnExistingNom(true, $unGroupe->getId(), $unGroupe->getNom())) {
            GestionErreurs::ajouter("Le Groupe " . $unGroupe->getNom() . " existe déjà");
        }
    }

}
